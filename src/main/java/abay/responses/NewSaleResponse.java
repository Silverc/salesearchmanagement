package abay.responses;

public class NewSaleResponse {
    private boolean success;

    public NewSaleResponse() {
    }

    public NewSaleResponse(boolean success) {
        this.success = success;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
