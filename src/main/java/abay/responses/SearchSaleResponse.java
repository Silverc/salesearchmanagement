
/**
 * Created by toshiba on 22/06/2020.
 */
package abay.responses;

import abay.entities.Sale;

import java.util.List;

public class SearchSaleResponse {
    //private boolean success;
    private List<Sale> correctSales;

    public SearchSaleResponse() {
    }

    /*public SearchSaleResponse(boolean success) {
        this.success = success;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }*/

    public SearchSaleResponse(List correctSales) {
        this.correctSales = correctSales;
    }

    public List<Sale> getSuccess() {
        return correctSales;
    }

    public void setSuccess(List correctSales) {
        this.correctSales = correctSales;
    }
}
