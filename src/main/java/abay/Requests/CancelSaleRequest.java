package abay.Requests;

import java.util.concurrent.atomic.AtomicInteger;

public class CancelSaleRequest {

    private AtomicInteger id;

    public CancelSaleRequest(){
    }

    public CancelSaleRequest(AtomicInteger id){this.id=id;
    }

    public AtomicInteger getId() {
        return id;
    }

    public void setId(AtomicInteger id) {
        this.id = id;
    }
}
