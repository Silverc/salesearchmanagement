package abay.Requests;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ModifySaleRequest {


    //public boolean modifySale(AtomicInteger id, List<String> productCategories, String productDescription, Integer productQuantity, Date limitDate, BigDecimal unitaryPrice,String productCondition){

    private AtomicInteger id;
    private List<String> productCategories;
    private String productDescription;
    private Integer productQuantity;
    private Date limitDate;
    private BigDecimal unitaryPrice;
    private String productCondition;

    public ModifySaleRequest(){
    }

    public AtomicInteger getId() {
        return id;
    }

    public List<String> getProductCategories() {
        return productCategories;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public Integer getProductQuantity() {
        return productQuantity;
    }

    public Date getLimitDate() {
        return limitDate;
    }

    public BigDecimal getUnitaryPrice() {
        return unitaryPrice;
    }

    public String getProductCondition() {
        return productCondition;
    }

    public void setId(AtomicInteger id) {
        this.id = id;
    }

    public void setProductCategories(List<String> productCategories) {
        this.productCategories = productCategories;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }

    public void setLimitDate(Date limitDate) {
        this.limitDate = limitDate;
    }

    public void setUnitaryPrice(BigDecimal unitaryPrice) {
        this.unitaryPrice = unitaryPrice;
    }

    public void setProductCondition(String productCondition) {
        this.productCondition = productCondition;
    }
}
