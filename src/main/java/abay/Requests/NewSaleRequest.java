package abay.Requests;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class NewSaleRequest {
        private List<String> productCategories;
        private String productDescription;
        private Integer productQuantity;
        private String status;
        private Date limitDate;
        private BigDecimal unitaryPrice;
        private String seller;
        private String type;
        private String productCondition;

    public NewSaleRequest() {
    }

    public List<String> getProductCategories() {
        return productCategories;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public Integer getProductQuantity() {
        return productQuantity;
    }

    public String getStatus() {
        return status;
    }

    public Date getLimitDate() {
        return limitDate;
    }

    public BigDecimal getUnitaryPrice() {
        return unitaryPrice;
    }

    public String getSeller() {
        return seller;
    }

    public String getType() {
        return type;
    }

    public String getProductCondition() {
        return productCondition;
    }

    public void setProductCategories(List<String> productCategories) {
        this.productCategories = productCategories;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setLimitDate(Date limitDate) {
        this.limitDate = limitDate;
    }

    public void setUnitaryPrice(BigDecimal unitaryPrice) {
        this.unitaryPrice = unitaryPrice;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setProductCondition(String productCondition) {
        this.productCondition = productCondition;
    }
}
