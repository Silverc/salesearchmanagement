package abay.services;

import abay.entities.Sale;
import abay.repositories.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class SearchService {
    //private List<Sale> sales = new ArrayList<Sale>();

    final SaleRepository saleRepository;

    @Autowired
    public SearchService(SaleRepository saleRepository){
        this.saleRepository=saleRepository;
    }

    public List<Sale> getSales() {
        List<Sale> list = new ArrayList<>();
        saleRepository.findAll().forEach(e -> list.add(e));
        return list;
    }


    public List<Sale> getSalesByCategory(String category) {
        List<Sale> list = new ArrayList<>();
        saleRepository.findSaleByCategory(category).forEach(e -> list.add(e));
        return list;
    }

    public List<Sale> getSalesByStatus(String status) {
        List<Sale> list = new ArrayList<>();
        saleRepository.findSaleByStatus(status).forEach(e -> list.add(e));
        return list;
    }


}

