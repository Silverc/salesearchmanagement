package abay.entities;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Sale
 */
@Entity
public class Sale {

    public static String[] SaleStatuses = {
            "active",
            "sold",
            "cancelled",
            "closed"
    };

    public static String[] SaleTypes = {
            "sale",
            "auction"
    };

    public static String[] ProductCondition = {
            "new",
            "used"
    };

    /*public static String[] ProductCategories = {
            "laptops",
            "smartphones",
            "computers"
    };*/

    @Column
    @ElementCollection(targetClass=String.class)
    private List<String> productCategories;
    private String productDescription;
    private int productQuantity;
    private String status;
    private Date limitDate;
    private BigDecimal unitaryPrice;
    private String seller;
    private String type;
    private String productCondition;
    @Id
    private AtomicInteger id = new AtomicInteger();

    protected Sale() {
    }

    public Sale(List<String> productCategories,
                String productDescription,
                int productQuantity,
                String status,
                Date limitDate,
                BigDecimal unitaryPrice,
                String seller,
                String type,
                String productCondition) {
        this.productCategories = productCategories;
        this.productDescription = productDescription;
        this.productQuantity = productQuantity;
        this.status = status;
        this.limitDate = limitDate;
        this.unitaryPrice = unitaryPrice;
        this.seller = seller;
        this.type = type;
        this.productCondition = productCondition;
        //id.incrementAndGet();
    }

    public List<String> getProductCategories() {
        return productCategories;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public String getStatus() {
        return status;
    }

    public Date getLimitDate() {
        return limitDate;
    }

    public BigDecimal getUnitaryPrice() {
        return unitaryPrice;
    }

    public String getSeller() {
        return seller;
    }

    public String getType() {
        return type;
    }

    public String getProductCondition() {
        return productCondition;
    }

    public AtomicInteger getId() {
        return id;
    }

    public void setId(AtomicInteger id) {
        this.id = id;
    }

    public void setProductCategories(List<String> productCategories) {
        this.productCategories = productCategories;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setLimitDate(Date limitDate) {
        this.limitDate = limitDate;
    }

    public void setUnitaryPrice(BigDecimal unitaryPrice) {
        this.unitaryPrice = unitaryPrice;
    }

    public void setProductCondition(String productCondition) {
        this.productCondition = productCondition;
    }

}