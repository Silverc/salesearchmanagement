package abay.controllers;

import abay.Requests.CancelSaleRequest;
import abay.Requests.ModifySaleRequest;
import abay.Requests.NewSaleRequest;
import abay.entities.Sale;
import abay.responses.CancelSaleResponse;
import abay.responses.ModifySaleResponse;
import abay.responses.NewSaleResponse;
import abay.services.SaleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Controller
public class SaleController {

    final SaleManager saleManager;

    @Autowired
    public SaleController(SaleManager saleManager) {
        this.saleManager = saleManager;
    }

    @PostMapping(value = "/newSale")
    @ResponseBody
    public NewSaleResponse handleNewSale(@RequestBody NewSaleRequest newSaleRequest) {

        boolean success = false;

        if (newSaleRequest.getSeller() != null && newSaleRequest.getType()!=null && newSaleRequest.getUnitaryPrice()!=null
                && newSaleRequest.getProductQuantity()!=null && newSaleRequest.getProductDescription()!=null
                && newSaleRequest.getProductCategories()!=null) {

            Sale sale=new Sale(newSaleRequest.getProductCategories(),
                    newSaleRequest.getProductDescription(),
                    newSaleRequest.getProductQuantity(),
                    newSaleRequest.getStatus(),
                    newSaleRequest.getLimitDate(),
                    newSaleRequest.getUnitaryPrice(),
                    newSaleRequest.getSeller(),
                    newSaleRequest.getType(),
                    newSaleRequest.getProductCondition());

            success=saleManager.addSale(sale);
        }

        return new NewSaleResponse(success);
    }

    @PostMapping(path = "/cancelSale")
    public @ResponseBody
    CancelSaleResponse handleCancelSale(@RequestBody CancelSaleRequest cancelSaleRequest) {

        boolean success = false;
        AtomicInteger id=cancelSaleRequest.getId();
        if (id != null) {
            success=saleManager.cancelSale(id);
        }


        return new CancelSaleResponse(success);
    }

    @PostMapping(value = "/modifySale")
    public @ResponseBody
    ModifySaleResponse handleModifySale(@RequestBody ModifySaleRequest modifySaleRequest) {

        boolean success = false;
        AtomicInteger id=modifySaleRequest.getId();
        List<String> productCategories=modifySaleRequest.getProductCategories();
        String productDescription=modifySaleRequest.getProductDescription();
        Integer productQuantity=modifySaleRequest.getProductQuantity();
        Date limitDate=modifySaleRequest.getLimitDate();
        BigDecimal unitaryPrice= modifySaleRequest.getUnitaryPrice();
        String productCondition=modifySaleRequest.getProductCondition();

        if (id != null) {
            success=saleManager.modifySale(id,productCategories,productDescription,productQuantity,limitDate,unitaryPrice,productCondition);
        }

        return new ModifySaleResponse(success);
    }
}
