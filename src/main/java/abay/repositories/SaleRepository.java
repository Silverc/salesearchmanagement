package abay.repositories;

import abay.entities.Sale;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Repository
public interface SaleRepository extends CrudRepository<Sale, AtomicInteger> {

    @Override
    @Transactional(timeout = 8)
    Iterable<Sale> findAll();

    @Query("SELECT e from Sale e where :category in productCategories")
    List<Sale> findSaleByCategory(String category);

    @Query("select e from Sale e where status= :status")
    List<Sale> findSaleByStatus(String status);

    List<Sale> findSaleByType(String type);

    Sale findByid(AtomicInteger id);
}
