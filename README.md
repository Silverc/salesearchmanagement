Running Docker:
1.Run the "docker pull postgres" command to pull the postgres image


2.Run the following command to set up the postgres container
"docker run --name postgres -e POSTGRES_PASSWORD=Thunder500 -e POSTGRES_USER=postgres -e POSTGRES_DB=sale-service -d -p 5432:5432 postgres"

3.Run the following command to link the system image to the postgres one
"docker run --link postgres carlosmanuelfigueiredo/salesearch_manager:1.0"



Running docker-compose.yml:

This can be ran by typing the command: "docker-compose up".

This file is structured in the following way:
It has two systems: "db" (that deals with the database) and "app" (that deals with the RestApplication).

Both have their ports specified (db has port 5432 exposed and app has port 8080 exposed).

Db runs on the postgres official image and "app" runs on the "carlosmanuelfigueiredo/salesearch_manager" image.

The "app" system depends on the "db" system, as such "db" is executed first.



CI/CD Pipeline:
This was done entirely through Gitlab

First, there was the need to get an ssh key, that was gotten in this repository (https://gitlab.com/pedro.salgueiro/rest-jpa-test) and associated to the variable $SSH_PRIVATE_KEY 

The .gitlab-ci.yml was adapted from the .gitlab-ci.yml in "https://gitlab.com/pedro.salgueiro/rest-jpa-test", changing the docker repository it would interact with from "app" to "salesearch-service"

For .gitlab-ci.yml to work, there were needed two scripts: setup_env.sh and deploy.sh, these being copied from the same repository as mentioned above

The docker-compose.pog.yml was made to be a copy of the docker-compose.yml already in the code, these needing the following changes: 
- the image in the service "app" would be changed to "esue1920/salesearch-service"
- the port exposed in the image "app" would be changed to 8084, as it was on of the final ports (8084 and 8085) that were available for the project
- the ports open in the service "db" would be changed to 5434, as these were the only ones that didn't seem to be ocupied.

After the pipeline ran sucessfully, there was no change in the repository provided :"https://hub.docker.com/repository/docker/esue1920/salesearch-service"