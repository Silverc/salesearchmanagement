FROM openjdk:8-jdk-alpine
WORKDIR /
ADD target/sale-service-0.0.1-SNAPSHOT.jar sale-service.jar
CMD java -jar sale-service.jar
